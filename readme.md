# Лабораторная работа №5

## **Docker: Мультистейджинг, различные уровни зависимостей**

Цель лабораторной: освоить методы создания образов с разными уровнями зависимостей

1. Для системных зависимостей приложения создаем образ name:system
2. Для зависимостей сборки создаем образ name:build
3. Для приложения создаем образ name:app
4. Вся конфигурация выполняется через переменные окружения
5. Освоить параметр --cache-from
6. Образ должен быть на базе scratch

Собрать Docker-образы:

   ```console
   $ docker build -t name:system -f Dockerfile.system .
   $ docker build --cache-from name:system -t name:build -f Dockerfile.build .
   $ docker build --cache-from name:build -t name:app -f Dockerfile.app .
   ```

Запустить контейнер из созданного образа:

   ```console
   $ docker run -e PORT=8080 -t -p 8080:8080 name:app
   ```