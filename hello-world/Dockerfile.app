FROM python:3.10-slim-buster

COPY --from=name:build /opt/venv /opt/venv
WORKDIR /app
COPY main.py .

ENTRYPOINT ["/opt/venv/bin/python3", "main.py"]
