from aiohttp import web
import os


async def handle(request):
    name = request.match_info.get('name', "World!")
    text = "Hello, " + name
    print('received request, replying with "{}".'.format(text))
    return web.Response(text=text)


app = web.Application()
app.router.add_get('/', handle)
app.router.add_get('/{name}', handle)

port = os.getenv("PORT", "8080")
web.run_app(app, port=int(port))
